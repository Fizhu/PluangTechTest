package com.fizhu.pluangtechnicaltest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.fizhu.pluangtechnicaltest.data.DataNegara;
import com.fizhu.pluangtechnicaltest.model.Negara;

import java.util.ArrayList;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {
    private Button save, cancel;
    private AutoCompleteTextView nama, ibu, foto;
    MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        save = findViewById(R.id.btsave);
        cancel = findViewById(R.id.btcancel);
        nama = findViewById(R.id.et_nama_negara);
        ibu = findViewById(R.id.et_ibu_kota);
        foto = findViewById(R.id.et_flag);

        save.setOnClickListener(this);
        cancel.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tambah Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == save) {
            String[][] data = new String[][]{
                    {nama.getText().toString(), ibu.getText().toString(), foto.getText().toString()}
            };

            Negara negara = null;
            ArrayList<Negara> list = new ArrayList<>();
            for (String[] aData : data){
                negara = new Negara();
                negara.setNamaNegara(aData[0]);
                negara.setNamaIbuKota(aData[1]);
                negara.setPoto(aData[2]);
                list.add(negara);
                }
            Intent i = new Intent(AddActivity.this, MainActivity.class);
            i.putParcelableArrayListExtra("LIST", list);
            startActivity(i);

            }
        if (v == cancel) {
            onBackPressed();
        }
    }
}
