package com.fizhu.pluangtechnicaltest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.fizhu.pluangtechnicaltest.R;
import com.fizhu.pluangtechnicaltest.WebActivity;
import com.squareup.picasso.Picasso;


public class DeltaFragment extends Fragment {
    private final String flag = "https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1280px-Flag_of_Germany.svg.png";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_delta, container, false);

        ImageView imageView = v.findViewById(R.id.img);
        Picasso.get().load(flag).into(imageView);

        FrameLayout l = v.findViewById(R.id.delta);
        l.setOnClickListener(v1 -> {
            Intent i = new Intent(getActivity(), WebActivity.class);
            i.putExtra("URL", "https://en.wikipedia.org/wiki/Germany");
            i.putExtra("TITLE", "Germany");
            startActivity(i);
        });

        return v;

    }
}
