package com.fizhu.pluangtechnicaltest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.fizhu.pluangtechnicaltest.R;
import com.fizhu.pluangtechnicaltest.WebActivity;
import com.squareup.picasso.Picasso;


public class GolfFragment extends Fragment {
    private final String flag = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/450px-Flag_of_Indonesia.svg.png";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_golf, container, false);

        ImageView imageView = v.findViewById(R.id.img);
        Picasso.get().load(flag).into(imageView);

        FrameLayout l = v.findViewById(R.id.golf);
        l.setOnClickListener(v1 -> {
            Intent i = new Intent(getActivity(), WebActivity.class);
            i.putExtra("URL", "https://id.wikipedia.org/wiki/Indonesia");
            i.putExtra("TITLE", "Indonesia");
            startActivity(i);
        });

        return v;

    }
}
