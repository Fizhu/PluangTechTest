package com.fizhu.pluangtechnicaltest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.fizhu.pluangtechnicaltest.R;
import com.fizhu.pluangtechnicaltest.WebActivity;
import com.squareup.picasso.Picasso;


public class CharlieFragment extends Fragment {
    private final String flag = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_Thailand.svg/1280px-Flag_of_Thailand.svg.png";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_charlie, container, false);

        ImageView imageView = v.findViewById(R.id.img);
        Picasso.get().load(flag).into(imageView);

        FrameLayout l = v.findViewById(R.id.charlie);
        l.setOnClickListener(v1 -> {
            Intent i = new Intent(getActivity(), WebActivity.class);
            i.putExtra("URL", "https://en.wikipedia.org/wiki/Thailand");
            i.putExtra("TITLE", "Thailand");
            startActivity(i);
        });

        return v;

    }
}
