package com.fizhu.pluangtechnicaltest.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.fizhu.pluangtechnicaltest.R;
import com.fizhu.pluangtechnicaltest.WebActivity;
import com.squareup.picasso.Picasso;


public class FoxtrotFragment extends Fragment {
    private final String flag = "https://upload.wikimedia.org/wikipedia/en/thumb/c/c3/Flag_of_France.svg/1280px-Flag_of_France.svg.png";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_foxtrot, container, false);

        ImageView imageView = v.findViewById(R.id.img);
        Picasso.get().load(flag).into(imageView);

        FrameLayout l = v.findViewById(R.id.foxtrot);
        l.setOnClickListener(v1 -> {
            Intent i = new Intent(getActivity(), WebActivity.class);
            i.putExtra("URL", "https://en.wikipedia.org/wiki/France");
            i.putExtra("TITLE", "France");
            startActivity(i);
        });

        return v;

    }
}
