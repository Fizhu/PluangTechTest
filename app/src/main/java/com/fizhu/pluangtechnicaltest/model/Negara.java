package com.fizhu.pluangtechnicaltest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fizhu on 20,September,2019
 */
public class Negara implements Parcelable {

    private String namaNegara, namaIbuKota, poto;
    private boolean expanded;

    public Negara() {
    }

    protected Negara(Parcel in) {
        namaNegara = in.readString();
        namaIbuKota = in.readString();
        poto = in.readString();
    }

    public static final Creator<Negara> CREATOR = new Creator<Negara>() {
        @Override
        public Negara createFromParcel(Parcel in) {
            return new Negara(in);
        }

        @Override
        public Negara[] newArray(int size) {
            return new Negara[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(namaNegara);
        dest.writeString(namaIbuKota);
        dest.writeString(poto);
    }

    public String getNamaNegara() {
        return namaNegara;
    }

    public void setNamaNegara(String namaNegara) {
        this.namaNegara = namaNegara;
    }

    public String getNamaIbuKota() {
        return namaIbuKota;
    }

    public void setNamaIbuKota(String namaIbuKota) {
        this.namaIbuKota = namaIbuKota;
    }

    public String getPoto() {
        return poto;
    }

    public void setPoto(String poto) {
        this.poto = poto;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public boolean isExpanded() {
        return expanded;
    }
}
