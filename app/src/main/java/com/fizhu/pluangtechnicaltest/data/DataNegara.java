package com.fizhu.pluangtechnicaltest.data;

import com.fizhu.pluangtechnicaltest.model.Negara;

import java.util.ArrayList;

/**
 * Created by fizhu on 20,September,2019
 */
public class DataNegara {

    public static String[][] data = new String[][]{
            {"Indonesia", "Jakarta", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/450px-Flag_of_Indonesia.svg.png"},
            {"Argentina", "Buenos Aires", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Flag_of_Argentina.svg/800px-Flag_of_Argentina.svg.png"},
            {"Singapore", "Singapore", "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Singapore.svg/1280px-Flag_of_Singapore.svg.png"},
            {"Thailand", "Bangkok", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_Thailand.svg/1280px-Flag_of_Thailand.svg.png"},
            {"Italy", "Rome", "https://upload.wikimedia.org/wikipedia/en/thumb/0/03/Flag_of_Italy.svg/1280px-Flag_of_Italy.svg.png"},
            {"Peru", "Lima", "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Flag_of_Peru.svg/1280px-Flag_of_Peru.svg.png"},
            {"Germany", "Berlin", "https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1280px-Flag_of_Germany.svg.png"}
    };

    public static ArrayList<Negara> getListData(){
        Negara negara = null;
        ArrayList<Negara> list = new ArrayList<>();
        for (String[] aData : data){
            negara = new Negara();
            negara.setNamaNegara(aData[0]);
            negara.setNamaIbuKota(aData[1]);
            negara.setPoto(aData[2]);
            list.add(negara);
        }
        return list;
    }
}
