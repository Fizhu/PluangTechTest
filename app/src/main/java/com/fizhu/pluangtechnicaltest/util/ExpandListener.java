package com.fizhu.pluangtechnicaltest.util;

public interface ExpandListener {
    void onExpandComplete();
    void onCollapseComplete();
}

