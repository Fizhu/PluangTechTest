package com.fizhu.pluangtechnicaltest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.fizhu.pluangtechnicaltest.adapter.BannerAdapter;
import com.fizhu.pluangtechnicaltest.adapter.RecyclerViewAdapter;
import com.fizhu.pluangtechnicaltest.data.DataNegara;
import com.fizhu.pluangtechnicaltest.model.Negara;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    DotsIndicator dotsIndicator;
    RecyclerViewAdapter adapter;
    FloatingActionButton floatingActionButton;

    public ArrayList<Negara> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        floatingActionButton = findViewById(R.id.fab);
        dotsIndicator = findViewById(R.id.dots_indicator);
        viewPager = findViewById(R.id.banner);
        BannerAdapter bannerAdapter = new BannerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(bannerAdapter);
        dotsIndicator.setViewPager(viewPager);

        floatingActionButton.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, AddActivity.class)));

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

        RecyclerView recyclerView = findViewById(R.id.rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list.addAll(DataNegara.getListData());

        ArrayList<Negara> add = getIntent().getParcelableArrayListExtra("LIST");
        if (add != null) {
            list.addAll(add);
        }

        adapter = new RecyclerViewAdapter(recyclerView);
        adapter.setLisNegara(list);
        recyclerView.setAdapter(adapter);
    }
    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            MainActivity.this.runOnUiThread(() -> {
                if (viewPager.getCurrentItem() < 8 - 1) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            });
        }
    }
}
