package com.fizhu.pluangtechnicaltest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fizhu.pluangtechnicaltest.R;
import com.fizhu.pluangtechnicaltest.model.Negara;
import com.fizhu.pluangtechnicaltest.util.ExpandListener;
import com.fizhu.pluangtechnicaltest.util.ExpandableLinearLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private ArrayList<Negara> lisNegara;
    private RecyclerView recyclerView;
    private int lastExpandedCardPosition;
    private Context context;

    public ArrayList<Negara> getLisNegara() {
        return lisNegara;
    }

    public void setLisNegara(ArrayList<Negara> lisNegara) {
        this.lisNegara = lisNegara;
    }

    public RecyclerViewAdapter(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.card_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Negara negara = getLisNegara().get(position);

        holder.tvNama.setText(negara.getNamaNegara());
        holder.tvNamaex.setText(negara.getNamaNegara());
        holder.tvIbuKota.setText(negara.getNamaIbuKota());
        Picasso.get().load(negara.getPoto()).into(holder.poto);

        if(lisNegara.get(position).isExpanded()){
            holder.expandableView.setVisibility(View.VISIBLE);
            holder.expandableView.setExpanded(true);
        }
        else{
            holder.expandableView.setVisibility(View.GONE);
            holder.expandableView.setExpanded(false);
        }
    }
    @Override
    public int getItemCount() {
        return lisNegara.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNama, tvNamaex, tvIbuKota;
        ImageView poto;
        ExpandableLinearLayout expandableView;
        ExpandListener expandListener = new ExpandListener() {
            @Override
            public void onExpandComplete() {
                if(lastExpandedCardPosition!=getAdapterPosition() && recyclerView.findViewHolderForAdapterPosition(lastExpandedCardPosition)!=null){
                    ((ExpandableLinearLayout)recyclerView.findViewHolderForAdapterPosition(lastExpandedCardPosition).itemView.findViewById(R.id.expandableView)).setExpanded(false);
                    lisNegara.get(lastExpandedCardPosition).setExpanded(false);
                    ((ExpandableLinearLayout)recyclerView.findViewHolderForAdapterPosition(lastExpandedCardPosition).itemView.findViewById(R.id.expandableView)).toggle();
                }
                else if(lastExpandedCardPosition!=getAdapterPosition() && recyclerView.findViewHolderForAdapterPosition(lastExpandedCardPosition)==null){
                    lisNegara.get(lastExpandedCardPosition).setExpanded(false);
                }
                lastExpandedCardPosition = getAdapterPosition();
            }

            @Override
            public void onCollapseComplete() {

            }
        };
        ViewHolder(View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.namaNegara);
            tvNamaex = itemView.findViewById(R.id.namaEx);
            tvIbuKota = itemView.findViewById(R.id.ibukota);
            poto = itemView.findViewById(R.id.poto);
            expandableView = itemView.findViewById(R.id.expandableView);
            expandableView.setExpandListener(expandListener);
            initializeClicks();
        }

        private void initializeClicks() {
            itemView.setOnClickListener(view -> {
                if (expandableView.isExpanded()) {
                    expandableView.setExpanded(false);
                    expandableView.toggle();
                    lisNegara.get(getAdapterPosition()).setExpanded(false);
                } else {
                    expandableView.setExpanded(true);
                    lisNegara.get(getAdapterPosition()).setExpanded(true);
                    expandableView.toggle();
                }
            });
        }
    }
}
