package com.fizhu.pluangtechnicaltest.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.fizhu.pluangtechnicaltest.fragments.AlphaFragment;
import com.fizhu.pluangtechnicaltest.fragments.BetaFragment;
import com.fizhu.pluangtechnicaltest.fragments.CharlieFragment;
import com.fizhu.pluangtechnicaltest.fragments.DeltaFragment;
import com.fizhu.pluangtechnicaltest.fragments.EchoFragment;
import com.fizhu.pluangtechnicaltest.fragments.FoxtrotFragment;
import com.fizhu.pluangtechnicaltest.fragments.GolfFragment;
import com.fizhu.pluangtechnicaltest.fragments.HotelFragment;

public class BannerAdapter extends FragmentPagerAdapter {
    public BannerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return new AlphaFragment();
        }if(position==1){
            return new BetaFragment();
        }if(position==2){
            return new CharlieFragment();
        }if(position==3) {
            return new DeltaFragment();
        }if(position==4) {
            return new EchoFragment();
        }if(position==5) {
            return new FoxtrotFragment();
        }if(position==6) {
            return new GolfFragment();
        }else {
            return new HotelFragment();
        }
    }

    @Override
    public int getCount() {
        return 8;
    }
}
